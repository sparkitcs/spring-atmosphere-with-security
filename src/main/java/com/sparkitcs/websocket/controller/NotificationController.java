package com.sparkitcs.websocket.controller;

import org.atmosphere.cpr.*;

import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
/**
 * Created by sparkitcs on 04/09/15.
 */
public class NotificationController {




    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String home(ModelMap model) {

        model.addAttribute("message", "Welcome!!!");

        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String name = user.getUsername(); //get logged in username

        model.addAttribute("username", name);

        return "hello";

    }

    @RequestMapping(value = "/pushMessage", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public void ring(@RequestParam(value="client", required=false) String client,
                     @RequestParam(value="payload", required=false) String payload,
                     HttpServletRequest request) {

        BroadcasterFactory bFactory = getBroadcasterFactory(request);
        bFactory.lookup(client, true).broadcast(payload);

    }

    @RequestMapping(value = "/subscribe")
    @ResponseStatus(HttpStatus.OK)
    public void subscribe(
            @RequestParam("u") String userId,
            HttpServletRequest request) {


        //Atmosphere framework puts filter/servlet that adds ATMOSPHERE_RESOURCE to all requests
        AtmosphereResource resource = (AtmosphereResource) request.getAttribute(ApplicationConfig.ATMOSPHERE_RESOURCE);

        //suspending resource to keep connection
        resource.suspend();

        BroadcasterFactory bFactory = getBroadcasterFactory(request);

        //find broadcaster, second parameter says to create broadcaster if it doesn't exist
        Broadcaster broadcaster = bFactory.lookup(userId, true);

        //saving resource for notifications
        broadcaster.addAtmosphereResource(resource);


    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(@RequestParam(value = "error", required = false) String error,
                              @RequestParam(value = "logout", required = false) String logout) {

        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error", "Invalid username and password!");
        }

        if (logout != null) {
            model.addObject("msg", "You've been logged out successfully.");
        }
        model.setViewName("login");

        return model;

    }

    private BroadcasterFactory broadcasterFactory = null;
    private BroadcasterFactory getBroadcasterFactory(HttpServletRequest request){

        if(broadcasterFactory == null){

            //Atmosphere framework puts filter/servlet that adds ATMOSPHERE_RESOURCE to all requests
            AtmosphereResource resource = (AtmosphereResource) request.getAttribute(ApplicationConfig.ATMOSPHERE_RESOURCE);

            broadcasterFactory = resource.getAtmosphereConfig().getBroadcasterFactory();

        }
        return broadcasterFactory;
    }

}
